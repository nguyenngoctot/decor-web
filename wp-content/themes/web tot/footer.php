		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="container">
				<div class="row">
					<div class="top_footer col-12">
						<div class="row">
							<div class="logo_footer col-md-5">
								<div class="logo">
									<img src="<?php the_field('logo_footer','options'); ?>" alt="icon_footer">
								</div>
								<div class="desc_ft">
									<p><?php the_field('description_ft','options'); ?></p>
								</div>
							</div>
							<div class="link_cat_product col-md-5">
								<h2 class="title">Our Shop</h2>
								<div class="content">
									<?php
										$taxonomy = 'product_cat';
 										$tax_terms = get_terms($taxonomy);
										?>
										<ul class="list_category">
										<?php
										foreach ($tax_terms as $tax_term) { ?>
											<li class="item">
												<a href="<?php echo get_term_link($tax_term, $taxonomy); ?>"><?php echo $tax_term->name; ?></a>
											</li>
										<?php } ?>
									</ul>
								</div>
							</div>
							<div class="quick_link col-md-2">
								<h2 class="title">Quick Links</h2>
								<div class="content">
									<?php add_footer_menu(); ?>
								</div>
							</div>
						</div>
					</div>
					<div class="bootom_footer col-12">
						<div class="row">
							<div class="bootom_footer_left col-md-4">
									<p class="address"><?php the_field('address','options'); ?></p>
							</div>
							<div class="bootom_footer_center col-md-4">
									<div class="form_email">
										<?php echo do_shortcode('[contact-form-7 id="172" title="Contact email bottom"]'); ?>
									</div>
							</div>
							<div class="bootom_footer_right col-md-4">
								<div class="email_number_phone">
									<p class="email"><?php the_field('email','options'); ?></p>
									<p class="number_phone"><?php the_field('number_phone','options'); ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
	
<?php wp_footer(); ?>

</body>
</html>
