jQuery(document).ready(function($){
    $('footer .bootom_footer .bootom_footer_center form .your-email').append('<span class="icon_submit"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>');
    $('footer .bootom_footer .bootom_footer_center form .icon_submit').click(function(){
        $('footer .bootom_footer .bootom_footer_center form .wpcf7-submit').trigger('click');
    });
    $('.contact-page .content_page_contact .contact_form form .wpcf7-submit').after('<span><i class="fa fa-location-arrow" aria-hidden="true"></i></span>');


    $('.product-home .list_product .woocommerce , .related_product .related').addClass('swiper-container');
    $('.product-home .list_product .woocommerce .products , .related_product .related .products ').addClass('swiper-wrapper');
    $('.product-home .list_product .woocommerce .products .product , .related_product .related .products product').addClass('swiper-slide');
    $('.product-home .list_product , div.related_product .container').append('<div class="swiper-button-next"></div><div class="swiper-button-prev"></div>');
    var swiper = new Swiper('.product-home .swiper-container', {
        spaceBetween: 70,
        slidesPerView: 4,
        // centeredSlides: true,
        breakpoints: {
            576: {slidesPerView: 2,spaceBetween: 10,},
            992: {slidesPerView: 3,spaceBetween: 20,},
            1366: {slidesPerView: 4,spaceBetween: 30,},
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
      });
      var swiper = new Swiper('.related_product .swiper-container', {
        spaceBetween: 70,
        slidesPerView: 4,
        // centeredSlides: true,
        breakpoints: {
            576: {slidesPerView: 2,spaceBetween: 10,},
            992: {slidesPerView: 3,spaceBetween: 20,},
            1366: {slidesPerView: 4,spaceBetween: 30,},
            1700: {slidesPerView: 4,spaceBetween: 70,},
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
      });
      var swiper = new Swiper('.list_these_rooms.swiper-container', {
        spaceBetween: 90,
        slidesPerView: 3,
        // centeredSlides: true,
        breakpoints: {
            576: {slidesPerView: 1,spaceBetween: 5,},
            992: {slidesPerView: 2,spaceBetween: 20,},
            1200: {slidesPerView: 3,spaceBetween: 30,},
            1700: {slidesPerView: 3,spaceBetween: 90,},
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
      });
      var swiper = new Swiper('.about-page .swiper-container', {
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
      });

      
      
      
      
      
      
     
});