
<?php
// global $product;
// echo $product-> ID ; 
function mytheme_add_woocommerce_support() {add_theme_support( 'woocommerce' );} add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' ); // Bật hỗ trợ woocommerce

function add_theme_scripts()
{
    $version = '1.0';

    wp_enqueue_script('StyleScript', get_template_directory_uri() . '/assets/js/style.js', array(), $version, true);
    wp_enqueue_script('Swiperjs', get_template_directory_uri() . '/assets/plugins/swiper/swiper-bundle.min.js', array(), $version, true);

    wp_enqueue_style('col', get_template_directory_uri() . '/assets/css/col.css', array(), $version, 'all');
    wp_enqueue_style('style', get_template_directory_uri() . '/assets/css/style.css', array(), $version, 'all');
    wp_enqueue_style('FontAwesome', get_template_directory_uri() . '/assets/css/fontAwesome/font-awesome.min.css', array(), $version, 'all');
    wp_enqueue_style('Swipercss', get_template_directory_uri() . '/assets/plugins/swiper/swiper-bundle.min.css', array(), $version, 'all');

    
}

add_action('wp_enqueue_scripts', 'add_theme_scripts');

//Menu
add_theme_support( 'menus' );
register_nav_menus(
    array(
            'main-nav' => 'Menu chính',
            'footer-nav' => 'Footer menu'
    )
);
function add_main_menu(){
    wp_nav_menu( 
        array(
            'theme_location' => 'main-nav', 
            'container' => 'nav', 
            'container_class' => 'menu-nav', 
            'menu_class' => 'mainmenu' 
        )
    );
}
function add_footer_menu(){
    wp_nav_menu( 
        array(
            'theme_location' => 'footer-nav', 
            'container' => 'nav', 
            'container_class' => 'menu-nav', 
            'menu_class' => 'footer-menu' 
        )
    );
}
//Theme Options
if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug'  => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect'   => false
    ));

    acf_add_options_sub_page(array(
        'page_title'  => 'Theme Header Settings',
        'menu_title'  => 'Header',
        'parent_slug' => 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title'  => 'Theme Footer Settings',
        'menu_title'  => 'Footer',
        'parent_slug' => 'theme-general-settings',
    ));
}
//sidebar
register_sidebar(array(
    'name' => 'Sidebar left',
    'id' => 'sidebar_left',
    'description' => 'Khu vực sidebar hiển thị bên trái trang',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h1 class="widget-title">',
    'after_title' => '</h1>'
));
//Remove hook
remove_action('woocommerce_before_main_content','woocommerce_breadcrumb',20);
remove_action('woocommerce_after_shop_loop_item','woocommerce_template_loop_add_to_cart',10);
remove_action('woocommerce_after_single_product_summary','woocommerce_output_related_products',20);
remove_action('woocommerce_single_product_summary','woocommerce_template_single_meta',40);
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );

//add action
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 20 );
add_action('woocommerce_related', 'woocommerce_output_related_products',10);
//add_action('breadcrumb_woocommerce', 'woocommerce_breadcrumb',10);
//woocommerce result countz


function woocommerce_result_count() {
    return;
   }
//
add_action( 'after_setup_theme', 'yourtheme_setup' );

function yourtheme_setup() {

    add_theme_support( 'wc-product-gallery-zoom' );

    add_theme_support( 'wc-product-gallery-lightbox' );

    add_theme_support( 'wc-product-gallery-slider' );

}
//add tabs
add_filter( 'woocommerce_product_tabs', 'flwoo_new_product_tab' );
function flwoo_new_product_tab( $tabs ) {
	$tabs['ctdt_tab'] = array(
		'title' => __( 'Installation', 'woocommerce' ),
		'priority' => 20,
		'callback' => 'noi_dung_tab_Installation'
		);
	return $tabs;
}
function noi_dung_tab_Installation() {
    ?>
        <p class="content"><?php the_field('installation',get_the_id()) ?></p>
    <?php
}

add_filter( 'woocommerce_product_tabs', 'flwoo_new_product_tabFQA' );
function flwoo_new_product_tabFQA( $tabs ) {
	$tabs['ctdt_tabfqa'] = array(
		'title' => __( 'FQA', 'woocommerce' ),
		'priority' => 50,
		'callback' => 'noi_dung_tab_FQA'
		);
	return $tabs;
}
function noi_dung_tab_FQA() {
  ?>
         <?php 
                        if(have_rows('fqa' , get_the_id() )){
                            while (have_rows('fqa' , get_the_id() )) : the_row();
                                $question= get_sub_field('question');
                                $answer= get_sub_field('answer'); 
                    ?>
                    <div class="item col-md-4 col-3">
                        <h2 class="question"><?= $question ?></h2>
                        <p class="answer"><?= $answer ?></p>
                    </div>
                    <?php
                            endwhile;
                        }
                    ?>
  <?php
}

add_filter( 'woocommerce_product_tabs', 'flwoo_new_product_tabTexture' );
function flwoo_new_product_tabTexture( $tabs ) {
	$tabs['ctdt_tab_Texture'] = array(
		'title' => __( 'Texture', 'woocommerce' ),
		'priority' => 30,
		'callback' => 'noi_dung_tab_Texture'
		);
	return $tabs;
}
function noi_dung_tab_Texture() {
    ?>
        <p class="content"><?php the_field('texture',get_the_id()) ?></p>
    <?php
}

add_filter( 'woocommerce_product_tabs', 'sap_xep_lai_tab');
function sap_xep_lai_tab($tabs) {
	$tabs['description']['priority'] = 10;
	$tabs['reviews']['priority'] = 40;
    return $tabs;
}
//remove additional_information
add_filter( 'woocommerce_product_tabs', 'bbloomer_remove_product_tabs', 9999 );
  
function bbloomer_remove_product_tabs( $tabs ) {
    unset( $tabs['additional_information'] ); 
    return $tabs;
}





