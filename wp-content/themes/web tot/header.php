<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
<style type="text/css">
	.header .mainmenu li.logo_header a{background-image: url(<?php the_field('logo_header','options'); ?>);}
</style>
</head>

<body <?php body_class(); ?>>
	<header id="masthead" class="site-header" role="banner">
		<?php if( wp_is_mobile()): ?> 
			<div class="header header_mobile">
				<div class="logo">
					<a href="/"><img src="<?php the_field('logo_header','options'); ?>" alt="logomobile"></a>
				</div>
				<div class="menu_mobile">
					<div class="icon_menu">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</div>
					<div class="menu">
						<div class="div_acount_mobile">
							<div class="acount_mobile row">
								<div class="icon_acount col-2">
									<i class="fa fa-user-circle" aria-hidden="true"></i>
								</div>
								<?php if (is_user_logged_in()): ?>  
									<?php $current_user = wp_get_current_user(); ?>
									<div class="display_hover_mobile col-10">
										<div class="helo_account_mobile">
											<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">
											<?php echo $current_user->display_name; ?></a></div>
										<div class="logout_mobile"><a href="<?php echo  wc_logout_url();?>"> Đăng xuất</a></div>
									</div>
									<?php else: ?>
									<div class="display_hover_mobile col-9">
										<div class="login_mobile"><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>"> Đăng Nhập</a></div>
										<div class="registration_mobile"><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>?dk=dangki"> Đăng kí</a></div>
									</div>
								<?php endif ?>
							</div>
						</div>
						<?php add_main_menu(); ?>
					</div>
				</div>
			</div>
		<?php  else: ?>
			<div class="header">
				<div class="row">
					<div class="header_left col-md-1">
						<span class="icon">
							<img src="<?php echo get_bloginfo('url') ?>/wp-content/themes/web tot/assets/images/ic_fb.png" alt="icon_header">
						</span>
						<span class="icon ic_ins">
							<img src="<?php echo get_bloginfo('url') ?>/wp-content/themes/web tot/assets/images/ic_ins.png" alt="icon_header">
						</span>
					</div>
					<div class="header_menu col-md-10">
						<?php add_main_menu(); ?>
					</div>
					<div class="header_right col-md-1">
						<div class="cart_acount">
							<div class="acount">
								<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">
									<img src="<?php echo get_bloginfo('url') ?>/wp-content/themes/web tot/assets/images/ic_acount.png" alt="icon_header">
								</a>
							</div>
							<div class="cart">
								<a href="/cart">
									<img src="<?php echo get_bloginfo('url') ?>/wp-content/themes/web tot/assets/images/ic_cart.png" alt="icon_header">
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php  endif;?>
		
	</header>