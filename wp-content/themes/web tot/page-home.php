<?php get_header(); ?>
    <div class="home">
         <section class="slide">
            <!-- <div class="swiper-container">
                <div class="swiper-wrapper"> -->
                <?php 
                    if(have_rows('box_slider')){
                        while (have_rows('box_slider')) : the_row();
                            $title= get_sub_field('title_slider');
                            $content= get_sub_field('content_slider');
                            $image= get_sub_field('img_slider'); 
                            $img_angle_must= get_sub_field('img_angle_must'); 
                ?>
                        <div class="swiper-slide">
                            <img src="<?= $image ?>" alt="img_slide">
                            <div class="content_slide">
                                <div class="contents">
                                    <h1 class="title"><?= $title ?></h1>
                                    <p class="content"><?= $content ?></p>
                                    <a href="" class="shop_now">Shop Now <span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
                                    <img src="<?= $img_angle_must ?>" alt="img_angle_must">
                                </div>
                            </div>
                        </div>
                <?php
                 endwhile;
                    }
                ?>
                <!-- </div>
            </div> -->
         </section>
         <section class="product-home">
             <div class="container">
                    <h1 class="title">Featured Products</h1>
                    <div class="list_product">
                        <?php echo do_shortcode( '[featured_products limit="8"]' );?>
                    </div>
             </div>
         </section>
         <section class="works">
             <div class="container">
                <h1 class="title">How It Works</h1>
                <div class="row">
                    <?php 
                        if(have_rows('how_it_work')){
                            while (have_rows('how_it_work')) : the_row();
                                $title= get_sub_field('title');
                                $content= get_sub_field('content');
                                $image= get_sub_field('image_icon'); 
                    ?>
                    <div class="item col-md-4 col-3">
                        <img src="<?= $image ?>" alt="ic_work">
                        <h2 class="title"><?= $title ?></h2>
                        <p class="content"><?= $content ?></p>
                    </div>
                    <?php
                            endwhile;
                        }
                    ?>
                </div>
             </div>
         </section>
         <section class="product_cat">
             <div class="container">
                 <h1 class="title">Our Collections</h1>
                 <div class="list_product_cat">
                     <div class="row">
                        <?php
                            $taxonomy = 'product_cat';
                            $tax_terms = get_terms($taxonomy);
                        ?>
                        <?php  foreach ($tax_terms as $key => $tax_term) { ?>
                                    <div class="item col-md-4 col-6">
                                        <div class="anh_sp_vnkings">
                                            <img src="<?php $thumbnail_id = get_woocommerce_term_meta( $tax_term->term_id, 'thumbnail_id', true ); echo $image = wp_get_attachment_url($thumbnail_id); ?>" alt="<?php echo $tax_term->name; ?>"/>
                                        </div>
                                        <div class="title_sp_vnkings">
                                            <a href="<?php echo get_term_link($tax_term, $taxonomy); ?>"><?php echo $tax_term->name; ?> <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                        <?php 
                            if ($key == 5){break;}
                        }?>
                        <a href="/all-category-product" class="view_all">View All</a>
                    </div>
                </div>
             </div>
         </section>
         <section class="about_us">
                 <?php 
                    if(have_rows('about_us')){
                        while (have_rows('about_us')) : the_row();
                            $title= get_sub_field('title');
                            $content= get_sub_field('content');
                            $image= get_sub_field('image'); 
                ?>
                    <div class="about">
                        <div class="row">
                            <div class="content_about col-md-6 col-12">
                                <div class="content">
                                    <h1 class="title"><?= $title ?></h1>
                                    <div class="desc"><?= $content ?></div>
                                    <div class="button">
                                        <a href="#">Learn More</a>
                                    </div>
                                </div>
                            </div>
                            <div class="image_about col-md-6 col-12">
                                    <img src="<?= $image ?>" alt="img_ab">
                            </div>
                        </div>
                    </div>
                <?php
                        endwhile;
                    }
                ?>
         </section>
    </div>
<?php get_footer(); ?>

