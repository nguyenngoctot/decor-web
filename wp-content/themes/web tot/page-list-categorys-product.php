<?php get_header(); ?>
    <div class="category-page">
        <section class="banner_top">
            <?php 
                if(have_rows('banner_shop','257')){
                    while (have_rows('banner_shop','257')) : the_row();
                        $title= get_sub_field('title');
                        $content= get_sub_field('description');
                        $image= get_sub_field('image'); 
            ?>
                            <div class="banner">
                                <img src="<?= $image ?>" alt="img_banner">
                                <div class="content">
                                    <h1 class="title"><?= $title; ?></h1>
                                    <p class="description"><?= $content ?></p>
                                </div>
                            </div>   
            <?php
                    endwhile; 
                }
            ?>
        </section>
        
        <section class="introduce_shop ">
            <div class="container row">
                    <?php 
                        if(have_rows('introduce_shop','257')){
                            while (have_rows('introduce_shop','257')) : the_row();
                                $title= get_sub_field('title');
                                $content= get_sub_field('description');
                                $image= get_sub_field('image'); 
                    ?>
                                    <div class="box_introduce col-12 col-md-6">
                                        <img src="<?= $image ?>" alt="img_banner">
                                        <div class="content">
                                            <div class="box">
                                                <h1 class="title"><?= $title ?></h1>
                                                <p class="description"><?= $content ?></p>
                                            </div>
                                            <a href="/shop/" class="shop_now">Shop Now <span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
                                        </div>
                                    </div>   
                    <?php
                            endwhile; 
                        }
                    ?>
            </div>
        </section>

        <section class="our_collectionS">
            <div class="container">
                <h1 class="title">Our Collections</h1>
                <div class="list_product_cat">
                     <div class="row">
                        <?php
                            $taxonomy = 'product_cat';
                            $tax_terms = get_terms($taxonomy);
                        ?>
                        <?php  foreach ($tax_terms as $key => $tax_term) { ?>
                                    <div class="item col-md-4 col-6">
                                        <div class="anh_sp_vnkings">
                                            <img src="<?php $thumbnail_id = get_woocommerce_term_meta( $tax_term->term_id, 'thumbnail_id', true ); echo $image = wp_get_attachment_url($thumbnail_id); ?>" alt="<?php echo $tax_term->name; ?>"/>
                                        </div>
                                        <div class="title_sp_vnkings">
                                            <a href="<?php echo get_term_link($tax_term, $taxonomy); ?>"><?php echo $tax_term->name; ?> <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                        <?php 
                            if ($key == 5){break;}
                        }?>
                        <a href="/all-category-product" class="view_all">View All</a>
                    </div>
                </div>
            </div>
        </section>

        <section class="Shop_by_Room">
            <div class="container">
                <h1 class="title">Shop by Room</h1>
                <div class="row">
                <?php 
                    $i=1;
                        if(have_rows('shop_by_room','257')){
                            while (have_rows('shop_by_room','257')) : the_row();
                                $title= get_sub_field('title');
                                $content= get_sub_field('description');
                                $image= get_sub_field('image'); 
                                $number= get_sub_field('number_of_products'); 
                                $link=get_sub_field('link'); 
                            if ($i==1) {
                               
                    ?>
                                    <div class="box_introduce box_introduce_first col-12 col-md-8">
                                        <div class="item item-<?= $i ?> col-12">
                                            <img src="<?= $image ?>" alt="img_banner">
                                            <div class="content">
                                                <div class="box">
                                                    <p class="number"><?= $number ?></p>
                                                    <h2 class="title"><?= $title ?></h2>
                                                    <p class="description"><?= $content ?></p>
                                                </div>
                                                <a href="<?= $link?>" class="Learn_More">Learn More<span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box_introduce col-12 col-md-4">   
                    <?php }else{ ?>
                                        <div class="item item-<?= $i ?> col-12">
                                            <img src="<?= $image ?>" alt="img_banner">
                                                <div class="content">
                                                    <div class="box">
                                                        <p class="number"><?= $number ?></p>
                                                        <h2 class="title"><?= $title ?></h2>
                                                    </div>
                                                    <a href="<?= $link?>" class="Learn_More">Learn More<span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
                                                </div>
                                        </div>
                     <?php  }
                         $i++ ;endwhile; 
                        }
                    ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="works">
             <div class="container">
                <h1 class="title">How It Works</h1>
                <div class="row">
                    <?php 
                        if(have_rows('how_it_work')){
                            while (have_rows('how_it_work')) : the_row();
                                $title= get_sub_field('title');
                                $content= get_sub_field('content');
                                $image= get_sub_field('image_icon'); 
                    ?>
                    <div class="item col-md-4 col-3">
                        <img src="<?= $image ?>" alt="ic_work">
                        <h2 class="title"><?= $title ?></h2>
                        <p class="content"><?= $content ?></p>
                    </div>
                    <?php
                            endwhile;
                        }
                    ?>
                </div>
             </div>
        </section>
    </div>
<?php get_footer(); ?>