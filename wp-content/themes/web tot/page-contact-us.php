<?php get_header(); ?>
    <div class="contact-page">
    <section class="banner_top">
            <?php 
                    if(have_rows('banner')){
                        while (have_rows('banner')) : the_row();
                            $title= get_sub_field('title');
                            $content= get_sub_field('description');
                            $image= get_sub_field('image'); 
                ?>
                     <div class="banner">
                         <img src="<?= $image ?>" alt="img_banner">
                         <div class="content">
                             <h1 class="title"><?= $title ?></h1>
                             <p class="description"><?= $content ?></p>
                         </div>
                     </div>   
                <?php
                 endwhile;
                    }
                ?>
    </section>
    <section class="content_page_contact">
        <div class="content_page">
            <div class="container row">
                <div class="contact_form col-12 col-md-8">
                    <?php echo do_shortcode( '[contact-form-7 id="81" title="Contact form 1"]' ); ?>
                </div>
                <div class="contact_form_right col-12 col-md-4">
                    <div class="back-color">
                        <div class="information_form">
                            <p class="email"><span>Email:<br></span>
                                <?php the_field('email','options'); ?>
                            </p>
                            <p class="phone"><span>Phone:<br></span>
                                <?php the_field('number_phone','options'); ?>
                            </p>
                            <p class="Address"><span>Address:</span>
                                <?php the_field('address','options'); ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>
<?php get_footer(); ?>