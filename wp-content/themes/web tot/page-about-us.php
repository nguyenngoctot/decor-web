<?php get_header(); ?>
    <div class="about-page">
        <section class="banner_top">
            <?php 
                    if(have_rows('banner_top_about')){
                        while (have_rows('banner_top_about')) : the_row();
                            $title= get_sub_field('title');
                            $content= get_sub_field('description');
                            $image= get_sub_field('image'); 
                ?>
                     <div class="banner">
                         <img src="<?= $image ?>" alt="img_banner">
                         <div class="content">
                             <h1 class="title"><?= $title ?></h1>
                             <p class="description"><?= $content ?></p>
                         </div>
                     </div>   
                <?php
                 endwhile;
                    }
                ?>
        </section>
        <section class="content_page">
                <?php 
                        if(have_rows('content_page')){
                            while (have_rows('content_page')) : the_row();
                                $title= get_sub_field('title');
                                $content= get_sub_field('description');
                                $image= get_sub_field('image'); 
                    ?>
                        <div class="content">
                            <div class="container">
                                <div class="row">
                                    <div class="content_left col-12 col-md-5">
                                        <img src="<?= $image ?>" alt="img_banner">
                                    </div>
                                    <div class="content_right col-12 col-md-7">
                                        <div class="box_content_right">
                                            <h2 class="title"><?= $title ?></h2>
                                            <p class="description"><?= $content ?></p>
                                            <a href="#" class="contact_us">Contact Us<span><i class="fa fa-angle-right" aria-hidden="true"></i></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                        endwhile;
                        }
                    ?>
        </section>
        <section class="slide_page" style="background-image: url(<?php the_field('backgound_slide') ?>);">
            <div class="slider">
                <h1 class="title">Testimonial</h1>
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <?php 
                            if(have_rows('slide_about')){
                                while (have_rows('slide_about')) : the_row();
                                    $content= get_sub_field('content');
                                    $name= get_sub_field('name'); 
                        ?>
                        <div class="swiper-slide">
                            
                            <div class="content">
                                <p class="icon"><i>"</i></p>
                                <?= $content ?>
                            </div>
                            <div class="name">
                                <p class="start-ic">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </p>
                                <h2 class="name"><?= $name ?></h2>
                            </div>
                        </div>
                        <?php
                        endwhile;
                            }
                        ?>
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            </div>
        </section>
    </div>
<?php get_footer(); ?>