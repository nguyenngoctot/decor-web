<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
	<div class="container">
		
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
			}
		?>
	</div>
	<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
		<div class="info_product ">
			<div class="container row">
				<div class="thumbnail_left col-12 col-md-5">
					<?php
					/**
					 * Hook: woocommerce_before_single_product_summary.
					 *
					 * @hooked woocommerce_show_product_sale_flash - 10
					 * @hooked woocommerce_show_product_images - 20
					 */
					do_action( 'woocommerce_before_single_product_summary' );
					?>
				</div>
				<div class="infomation_right col-12 col-md-7">
					<div class="summary entry-summary">
						<?php
						/**
						 * Hook: woocommerce_single_product_summary.
						 *
						 * @hooked woocommerce_template_single_title - 5
						 * @hooked woocommerce_template_single_rating - 10
						 * @hooked woocommerce_template_single_price - 10
						 * @hooked woocommerce_template_single_excerpt - 20
						 * @hooked woocommerce_template_single_add_to_cart - 30
						 * @hooked woocommerce_template_single_meta - 40
						 * @hooked woocommerce_template_single_sharing - 50
						 * @hooked WC_Structured_Data::generate_product_data() - 60
						 */
						do_action( 'woocommerce_single_product_summary' );
						?>
					</div>
				</div>
			</div>
		</div>
		<div class="tabs_product">
			<div class="container">
				<?php
				/**
				 * Hook: woocommerce_after_single_product_summary.
				 *
				 * @hooked woocommerce_output_product_data_tabs - 10
				 * @hooked woocommerce_upsell_display - 15
				 * @hooked woocommerce_output_related_products - 20
				 */
				do_action( 'woocommerce_after_single_product_summary' ); ?>
				<div class="nice_fit_in_these_rooms">
					<h1 class="title">Nice fit in these rooms</h1>
					<div class="list_these_rooms swiper-container">
						<div class="swiper-wrapper">
							<?php 
								if(have_rows('nice_fit_in_these_rooms', get_the_ID() )){
									while (have_rows('nice_fit_in_these_rooms', get_the_ID() )) : the_row();
										$title= get_sub_field('title');
										$content= get_sub_field('description');
										$image= get_sub_field('image'); 
							?>
											<div class="item swiper-slide">
												<img src="<?= $image ?>" alt="img_banner">
												<div class="content">
													<h1 class="title"><?= $title; ?></h1>
													<p class="description"><?= $content ?></p>
												</div>
											</div>   
							<?php
									endwhile; 
								}
							?>
						</div>
					</div>
					<div class="swiper-button-next"></div>
    				<div class="swiper-button-prev"></div>
				</div>
			</div>
		</div>

		<div class="related_product">
			<div class="container">
				<?php do_action( 'woocommerce_related' );?>
			</div>
		</div>
	</div>
<?php do_action( 'woocommerce_after_single_product' ); ?>

<script>
	jQuery(document).ready(function($){
		//single product
		var prices =  "<?php echo $price = get_post_meta( get_the_ID(), '_regular_price',true); ?>"
		$(document).on('keyup , change' ,  'input#number-1613651461294 , input#number-1613651632049 , form select.wcpa_use_sumo' , function(){
				var unit = $('form select.wcpa_use_sumo').val();
				var width = $('input#number-1613651461294').val();
				var height = $('input#number-1613651632049').val();
				if(unit == 'unit'){
					$price= width*height*prices
					$('#text-1613652603378').val($price+'$');
				}
				if(unit == 'sqm'){
					$price= width*height*prices
					$('#text-1613652603378').val($price+'$');
				}
				if(unit == 'cm'){
					$price= ((width*height)/10000)*prices
					$('#text-1613652603378').val($price+'$');
				}
				if(unit == 'mm'){
					$price= ((width*height)/1000000)*prices
					$('#text-1613652603378').val($price+'$');
				}
			});		
	});
</script>