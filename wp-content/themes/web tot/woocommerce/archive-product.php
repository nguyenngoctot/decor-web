<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

?>
<link rel="stylesheet" href="<?= home_url(); ?>/wp-content/themes/web tot/assets/scss/woocommer.css">

	 <section class="banner_top">
            <?php 
                if(have_rows('banner_shop','77')){
                    while (have_rows('banner_shop','77')) : the_row();
                        $title= get_sub_field('title');
                        $content= get_sub_field('description');
                        $image= get_sub_field('image'); 
            ?>
                            <div class="banner">
                                <img src="<?= $image ?>" alt="img_banner">
                                <div class="content">
                                    <h1 class="title"><?= $title; ?></h1>
                                    <p class="description"><?= $content ?></p>
                                </div>
                            </div>   
            <?php
                    endwhile; 
                }
            ?>
        </section>
	<div class="container">
	<?php
		if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb( '<p class="breadcrumbs">','</p>' );
		}
	?>
		<div class="row">
			<div class="sidebar-left col-12 col-md-4">
				<?php do_action( 'woocommerce_sidebar' ); ?>
			</div>
			<div class="content_page_shop col-12 col-md-8">
				<div class="banner_content">
					<?php 
						if(have_rows('banner_content_shop','77')){
							while (have_rows('banner_content_shop','77')) : the_row();
								$title= get_sub_field('title');
								$content= get_sub_field('description');
								$image= get_sub_field('image'); 
					?>
						
                                <img src="<?= $image ?>" alt="img_banner">
                                <div class="content-banner">
									<div class="content">
										<h2 class="title"><?= $title; ?></h2>
										<p class="description"><?= $content ?></p>
                            		</div>   
								</div>
					<?php
							endwhile; 
						}
					?>
				</div>
				<?php
					if ( woocommerce_product_loop() ) {

						/**
						 * Hook: woocommerce_before_shop_loop.
						 *
						 * @hooked woocommerce_output_all_notices - 10
						 * @hooked woocommerce_result_count - 20
						 * @hooked woocommerce_catalog_ordering - 30
						 */
						?>
						<div class="box-sort-phone">
							<div class="box-sort">
								<span>Sort By</span>
								<?php do_action( 'woocommerce_before_shop_loop' ); ?>
							</div>
							<div class="box-phone">
								<span>Need any help?</span>
								<div class="phones">
									<i class="fa fa-phone" aria-hidden="true"></i>
									<?php the_field('number_phone','options'); ?>
								</div>
							</div>
						</div>

						<?php
						woocommerce_product_loop_start();

						if ( wc_get_loop_prop( 'total' ) ) {
							while ( have_posts() ) {
								the_post();

								/**
								 * Hook: woocommerce_shop_loop.
								 */
								do_action( 'woocommerce_shop_loop' );

								wc_get_template_part( 'content', 'product' );
							}
						}

						woocommerce_product_loop_end();

						/**
						 * Hook: woocommerce_after_shop_loop.
						 *
						 * @hooked woocommerce_pagination - 10
						 */
						do_action( 'woocommerce_after_shop_loop' );
					} else {
						/**
						 * Hook: woocommerce_no_products_found.
						 *
						 * @hooked wc_no_products_found - 10
						 */
						do_action( 'woocommerce_no_products_found' );
					}
				?>
			</div>
		</div>
		


	</div>











		
		
		
<?php
get_footer( 'shop' );
