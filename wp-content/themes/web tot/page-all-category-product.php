<?php get_header(); ?>
    <div class="category-page category-all">
        <section class="banner_top">
            <?php 
                if(have_rows('banner_shop','289')){
                    while (have_rows('banner_shop','289')) : the_row();
                        $title= get_sub_field('title');
                        $content= get_sub_field('description');
                        $image= get_sub_field('image'); 
            ?>
                            <div class="banner">
                                <img src="<?= $image ?>" alt="img_banner">
                                <div class="content">
                                    <h1 class="title"><?= $title; ?></h1>
                                    <p class="description"><?= $content ?></p>
                                </div>
                            </div>   
            <?php
                    endwhile; 
                }
            ?>
        </section>
        <section class="our_collectionS">
            <div class="container">
                <div class="list_product_cat">
                     <div class="row">
                        <?php
                            $taxonomy = 'product_cat';
                            $tax_terms = get_terms($taxonomy);
                        ?>
                        <?php  foreach ($tax_terms as $key => $tax_term) { ?>
                                    <div class="item col-md-4 col-6">
                                        <div class="anh_sp_vnkings">
                                            <img src="<?php $thumbnail_id = get_woocommerce_term_meta( $tax_term->term_id, 'thumbnail_id', true ); echo $image = wp_get_attachment_url($thumbnail_id); ?>" alt="<?php echo $tax_term->name; ?>"/>
                                        </div>
                                        <div class="title_sp_vnkings">
                                            <a href="<?php echo get_term_link($tax_term, $taxonomy); ?>"><?php echo $tax_term->name; ?> <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                        <?php 
                           
                        }?>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php get_footer(); ?>