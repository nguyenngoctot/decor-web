<?php // File này cho trang page ?>
<?php get_header(); ?>
<div class="full-row full-content">
	<div id="primary" class="container">
		<?php if(is_page('cart') || is_page('checkout')||is_page('my-account')){?>
			<main id="main" class="content-wrap  page-wrap" role="main">
				<?php
				if ( have_posts() ) : while ( have_posts() ) : the_post();
					the_content();
				endwhile; endif;
				?>
			</main>
		<?php }else{ ?>
			<main id="main" class="content-wrap page-wrap" role="main">
				<?php
				if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<div class="row">
						<div class="col-md-3">
							<?php get_sidebar(); ?>
							fwertasdtrf
						</div>

						<div class="col-md-9">
							<header class="entry-header">
								<?php the_title( '<h1 class="page-heading ">', '</h1>' ); ?>
							</header>
							<div class="entry-content">
								<?php the_content(); ?>
							</div>
						</div>
					</div>
				<?php endwhile; endif;?>
			</main>
		<?php } ?>
	</div>
</div>	
<?php get_footer(); ?>