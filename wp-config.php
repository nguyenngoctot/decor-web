<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'web_test' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         't7&8) (rY=AVi@Ms-^` kV5fY7p)VnMK#a^#c|FYqfmlLZ(i)DLZ)M9&sHNgUCSC' );
define( 'SECURE_AUTH_KEY',  'b>KO&/f][ZTZ/8/dAMXj}F!; :Pry.&_-TQBTER4#SaJjSwIk@-nDqem@qUW<6#c' );
define( 'LOGGED_IN_KEY',    'c?^:g/;GalzX;<^_{;E4;>9e`rK[{!i7vpf9P1HuRPrV{^/+H7lc=R+a3FC=|:J ' );
define( 'NONCE_KEY',        'T0&{F*,<5Gi0:D6RS:ScZT:`C$h2KJ3V(k-%71|mo(X/,+=yGR 43{ymp3 <d$LT' );
define( 'AUTH_SALT',        '0&>eo:-Ka&k]G_@=,k%~z7uh]#Z^_W6a}c17?R*3M2N/}[$3 :c+i?n_N!C>F/SQ' );
define( 'SECURE_AUTH_SALT', 'g70B,_%=,Jo`M#4JIZs&dLj;+SqU.x5!Bm062e xYJJ,Wl`YSyR}SoN Jg}0l=$V' );
define( 'LOGGED_IN_SALT',   '~g(J3X nW6l&S ][n:XR{5[iLx7{cE}Y9uJvjVD3t4j7rsAqA$w{N{cHph i _cu' );
define( 'NONCE_SALT',       'd.Sjw^$wILvh6~9}5yzG,EE3[7NT&.1[v80;p(FQO)<nn+z?$vKC%KR=vv0&E/]Z' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
